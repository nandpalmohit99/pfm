<!DOCTYPE html>
<html>
<?php 
session_start();

include 'connection.php';



if(isset($_POST['forget'])) {
	extract($_POST);
	
	
        // print_r($data);
        $admin_name = admin;
        $admin_email = nandpalmohit@gmail.com;
        $otp = mt_rand(100000, 999999);
        $_SESSION['otp'] = $otp;
        $_SESSION['forget_email'] = $admin_email;

        $to = $email;
        $subject = "Forget Password by OTP";
        $msg = "Hello ".$admin_name." you otp is: ." .$otp;
        $headers = "From: nandpalmohit@gmail.com";

        mail($to,$subject,$msg,$headers);
        $_SESSION['myOtp']=$otp;
        $_SESSION['myEmail']=$email;

        header('location:otp.php');

    }
    else {
        echo "Wrong Email";
    }
}

 ?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Login Admin Panel - Personal Finance Management</title>
	<!-- Font Awesome icon -->
  	<script src="https://use.fontawesome.com/738dd8cf77.js"></script>
	<!-- Google Fonts-->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Karla:wght@200;300;400;500;600;700;800&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<!-- Vendor CSS Files -->
	<link href="/admin/assets/vendor/css/mdb.min.css" rel="stylesheet">
	<!-- Template CSS Files -->
	<link rel="stylesheet" href="/admin/assets/css/style.css">
</head>
<body>
	<section id="page">
		<div class="box dark-shadow">
			<div class="row" id="login">
				<div class="col-5 d-block bg-mydark py-4" id="page-welcome">
					<img src="/admin/assets/img/logo-icon.svg" width="250px" alt="">
					<p class="text-mypich"><i class="fa fa-check-circle" aria-hidden="true"></i> Secure and Private Admin Panel.</p>
					<p class="text-mypich"><i class="fa fa-check-circle" aria-hidden="true"></i> Login with username and password.</p>
					<p class="text-mypich"><i class="fa fa-check-circle" aria-hidden="true"></i> All rights reserved by PFM Admin.</p>
				</div>
				<div class="col bg-mydark" id="page-inputs">
					<div class="text-center pt-5 px-5">
						<h4 class="text-mypich">Welcome to Admin Panel</h4>
						<small class="text-mypich">Please enter Email id to reset your account</small>
						<form action="" method="post" accept-charset="utf-8" class="panel-card mt-5">
						  	<div class="form-outline my-5">
						    	<input type="email" name="admin_email" id="form1Example1" class="form-control" />
						    	<label class="form-label" for="form1Example1">Email</label>
						  	</div>
						  	<div class="form-outline text-center mt-0 mb-4">
						  		<button type="submit" name="forget" class="btn btn-light bg-btnlight">Submit</button>
						  	</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- script -->
  	<script src="/admin/assets/vendor/js/mdb.min.js"></script>
</body>
</html>
